# Sample FUSE 
This sample FUSE is for demonstrate how to externalized configuration and work with Jenkins and OpenShift

```

    ____           __   __  __      __     ________  _______ ______
   / __ \___  ____/ /  / / / /___ _/ /_   / ____/ / / / ___// ____/
  / /_/ / _ \/ __  /  / /_/ / __ `/ __/  / /_  / / / /\__ \/ __/   
 / _, _/  __/ /_/ /  / __  / /_/ / /_   / __/ / /_/ /___/ / /___   
/_/ |_|\___/\__,_/  /_/ /_/\__,_/\__/  /_/    \____//____/_____/   
                                                                   

                                                              
```
Table of Contents

* [Sample FUSE App](docs/FUSE.md)
* [Setup Demo](docs/SETUP.md)
* [Jenkins Pipeline for CI/CD](docs/JENKINS.md)


**Link to Gitlab:** [Here](https://gitlab.com/ocp-demo/sample-camel-with-jenkins.git)


