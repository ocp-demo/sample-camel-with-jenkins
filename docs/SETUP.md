# Setup Demo

<!-- TOC -->

- [Setup Demo](#setup-demo)
  - [Create Projects (Namespaces)](#create-projects-namespaces)
  - [Setup CI/CD Tools](#setup-cicd-tools)
  - [Create Pipeline](#create-pipeline)

<!-- /TOC -->

## Create Projects (Namespaces)

This demo consists of 3 projects

* CI/CD (ci-cd) - for tools including Jenkins (master/slave), Nexus and Sonarqube. All container images are stored in this namesapce
* Development (dev) - for developer
* UAT (uat) - for UAT

OpenShift has security to isolate each project then we need to

* Allow project dev and uat to pull images from ci-cd
* Allow project ci-cd to managing dev and uat
  
Shell script [setup_projects.sh](../bin/setup_projects.sh) will do all above jobs.


## Setup CI/CD Tools

Shell script [setup_cicd.sh](../bin/setup_cicd.sh) is provided for

* Deploy Jenkins server
* Deploy Nexus and setup repoistory for container images, maven and nodejs
* Deploy SonarQube
* Create custom maven 3.6.3 with skopeo for Jenkins slave

Remark: You need to change default Nexus password. Instruction will be provided at end of shell script's output.

## Create Pipeline

Create Build Pipeline on OpenShift

```
oc apply -f manifests/sample-build-pipeline.yaml -n ci-cd
oc apply -f manifests/sample-release-pipeline.yaml -n ci-cd
```
