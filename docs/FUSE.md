# Sample FUSE Application

<!-- TOC -->

- [Sample FUSE Application](#sample-fuse-application)
  - [Application configuration](#application-configuration)
  - [Externalized application configuration](#externalized-application-configuration)
  - [Mount ConfigMap into Pod](#mount-configmap-into-pod)

<!-- /TOC -->

## Application configuration

Application configuration can be stored in application.properties and refer in Spring DSL.

Check for [Camel Context](../code/src/main/resources/spring/camel-context.xml)

```
...
 <setBody id="route-setBody">
                <simple>***{{app.message}}***</simple>
 </setBody>
 ...
```

Check for [application.properties](../code/src/main/resources/application.properties)

```
...
# Application Configuration
app.message = This is message
...
```

## Externalized application configuration

SpringBoot can externalized properties file with following precedence.

* A /config subdirectory of the current directory
* The current directory
* A classpath /config package
* The classpath root

You can use **spring.config.location#configuration** with java parameter or **SPRING_CONFIG_LOCATION** as environment variable.

```

# For this example, Properties file located under directory configuration
 java -jar target/sample-spring-camel-1.0.0.jar \
  --spring.config.location#configuration/application.properties

# Use Environment Variable
export SPRING_CONFIG_LOCATION#configuration/application.properties
java -jar target/sample-spring-camel-1.0.0.jar 

```

## Mount ConfigMap into Pod

Secret can be mounted into Pod as mount path. This can be done with deployment config, deployment or by CLI.

Remark that ConfigMap is created by Jenkins Build Pipeline.

Check [deployment config](../manifests/sample-dc-dev.yaml) for secret and mount

```
...
# Inject Enviroment Variable to Pod
env:
 - name: SPRING_CONFIG_LOCATION
   value: ${SPRING_CONFIG_LOCATION}
...

# Define mount path
  volumeMounts:
  - mountPath: /config/application.properties
    name: config
    subPath: application.dev.properties
...

# Define volume as configMap
volumes:
- configMap:
    defaultMode: 420
    name: sample
  name: config  
...

```

You can also use CLI to create ConfigMap add to existing deployment config and deployment.

```
oc create -f sample-configmap-dev.yaml -n dev
oc set volume dc/sample --add --name=config --mount-path=/config/application.proprties --sub-path=application.properties --configmap-name=sample-camel -n dev

```

