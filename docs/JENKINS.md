# Jenkins Pipeline

<!-- TOC -->

- [Jenkins Pipeline](#jenkins-pipeline)
  - [Overview](#overview)
  - [Run Pipeline](#run-pipeline)
  - [Pipeline - Build](#pipeline---build)
  - [Pipeline - Release](#pipeline---release)

<!-- /TOC -->

## Overview

CI/CD flow as follow:

* Jenkins and image registry stored in CI/CD project
* Deploy to Dev project (namespace) by reading source code from Git then build container,tagged with version specified in POM concat with build number and deploy to Dev project.
* Deploy to UAT project by provided drop-down menu to select container tag, then tag again with current date (YYYYMMDD) and build number, deploy to UAT Project. 
* Deployment Config contains label tag to indicate which image is deployment config used.
* Config Map will be used for configure application with application.properties file. Then application in different project will have different configuration.

Check for Jenkinsfile

* [Build](../Jenkinsfile/build/Jenkinsfile)
* [Release](../Jenkinsfile/relese/Jenkinsfile)

## Run Pipeline

You can run pipeline from Jenkins, OpenShift Console or by CLI (oc command). Sample of oc command to run pipeline show below.

```

oc start-build sample-build-pipeline -n ci-cd
# Sample output
build.build.openshift.io/sample-build-pipeline-2 started
# Wait around 5 - 10 sec and run following command
oc logs build/sample-build-pipeline-2 -n ci-cd
# Sample output
info: logs available at /https://jenkins-ci-cd.apps.cluster-953d.953d.example.opentlc.com/blue/organizations/jenkins/ci-cd%2Fci-cd-sample-build-pipeline/detail/ci-cd-sample-build-pipeline/1/
# Open above URL for open Jenkins BlueOcean

```

## Pipeline - Build

Pipeline build will checkout current FUSE code from git and build container along with creating OpenShift objects incuding build config, imagesteam, deployment config, service and route.

Following show sample of the result from pipeline build for the first times.

![pipeline build 1](images/pipeline-build-1.png)

Following show sample of the result when re-run pipeline build. Remark that some steps will be skipped because that paticular object is already created. (Except for configmap that re-create everytime)

![pipeline build 2](images/pipelien-build-2.png)

Check OpenShift Dev console that pod is created in dev project

![pod in dev](images/pod-in-dev.png)


## Pipeline - Release

Pipeline release will deploy application based on container images that already build by Pipeline Build and tag container image again with current date (YYYYMMDD) and build number for refrence to this UAT round.

Pipeline will provided menu for select image version as follow

![pipeline release 1](images/pipeline-release-1.png)

Pipeline will always teardown everything before deploy

![pipeline release 2](images/pipeline-release-2.png)

Check OpenShift Dev console that pod is created in uat project

![pod in uat](images/pod-in-uat.png)



